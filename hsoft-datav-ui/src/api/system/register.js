import request from '@/utils/request'

//用户信息注册并发送邮件（新增用户信息）
export function register(data) {
    return request({
        url: '/system/register/register',
        method: 'post',
        data: data
    })
}

//修改用户激活标识
export function activation(code) {
    return request({
        url: '/system/register/activation' + code,
        method: 'get'
    })
}