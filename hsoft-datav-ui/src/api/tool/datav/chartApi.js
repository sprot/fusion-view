import axios from 'axios'
axios.defaults.headers['Content-Type'] = 'application/json;charset=utf-8'
import { getToken } from '@/utils/auth'
import request from '@/utils/request'
/**
 * 柱状图接口数据结构
 * @param {*} requestMethod 
 * @param {*} url 
 * @param {*} query 
 */

export function chartApiBar(requestMethod, url, query,token) {
  let data = {};
  data.method = requestMethod;
  data.url = url;
  data.query = query;
  data.token = token;

  return new Promise((resolve, reject) => {

    let config = {
      headers: {
        'Authorization':'Bearer ' + getToken()
      },
      
    }

   axios.post(process.env.VUE_APP_BASE_API+"/chart/transit/do", data,config).then(response => {
          if (response.data.code === 200) {

            // console.log(response.data)

            resolve(response.data.data);
          } else {
            alert("请求失败");
          }
        })
        .catch(function (error) {
          console.log(error);
        });

  })

  // return new Promise((resolve, reject) => {
  //     if(requestMethod == 'GET') {
  //       axios.get(url, query).then(response => {
  //         if (response.data.code === 200) {
  //           resolve(response.data.data);
  //         } else {
  //           alert("请求失败");
  //         }
  //       })
  //       .catch(function (error) {
  //         console.log(error);
  //       });
        
  //     } else if(requestMethod == 'POST') {
  //       axios.post(url, query).then(response => {
  //         if (response.data.code === 200) {
  //           resolve(response.data.data);
  //         } else {
  //           alert("请求失败");
  //         }
  //       })
  //       .catch(function (error) {
  //         console.log(error);
  //       });
  //     }
  // })
    
}

export function remoteChartApi(data) {
  return request({
    url: '/chart/transit/remote',
    method: 'post',
    data: data
  })
}

export function remoteTabApi(data) {
  return request({
    url: '/chart/transit/tab/remote',
    method: 'post',
    data: data
  })
}
export function chartBIanalysis(query) {
  return request({
      url: '/chart/BI/analysis',
      method: 'post',
      data: query
  })
}
