## 私有化部署

  >私有部署是指将整套服务安装部署到您的企业内网中。可以对接企业内网的数据源，无缝集成您的企业内部办公环境，具有更高的安全性和可控性。私有部署版具有当部署前版本的所有功能，并且对空间组件数目、报表个数、大屏个数、公开分享的大屏和报表个数不做任何限制，但不支持服务更新。